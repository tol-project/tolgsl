#include "TolGslEigen.h"
#include <gsl/gsl_eigen.h>

int TolGsl_Eigen_NonSymmetric(BMatrix<BDat> &A,
                              BMatrix<BDat> &Eigen_Real,
                              BMatrix<BDat> &Eigen_Imag)
{
  gsl_matrix *gslA = NULL;
  bmat_to_gsl(A, gslA);
  int n = A.Rows();
  gsl_vector_complex *eval = gsl_vector_complex_alloc(n);
  gsl_eigen_nonsymm_workspace *w = gsl_eigen_nonsymm_alloc(n);
  int status = gsl_eigen_nonsymm(gslA, eval, w);
  if (status != GSL_SUCCESS)
    {
    return status;
    }
  gsl_eigen_nonsymm_free(w);
  status = gsl_eigen_nonsymmv_sort(eval, NULL, GSL_EIGEN_SORT_ABS_DESC);
  if (status != GSL_SUCCESS)
    {
    return status;
    }
  gsl_vector_view view_real = gsl_vector_complex_real(eval);
  gsl_vector_view view_imag = gsl_vector_complex_imag(eval);
  gsl_to_bmat(&view_real.vector, Eigen_Real);
  gsl_to_bmat(&view_imag.vector, Eigen_Imag);
  gsl_vector_complex_free(eval);
  gsl_matrix_free(gslA);
  return GSL_SUCCESS;
}
