#ifndef _TolGslEigen_H
#define _TolGslEigen_H

#include "tol/tol_gslmat.h"

int TolGsl_Eigen_NonSymmetric(BMatrix<BDat> &A,
                              BMatrix<BDat> &Eigen_Real,
                              BMatrix<BDat> &Eigen_Img);

#endif
